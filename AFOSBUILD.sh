rm -rf /opt/ANDRAX/scada-tools

mkdir /opt/ANDRAX/scada-tools

cp -Rf *.py *.txt *.nse /opt/ANDRAX/scada-tools

cp -Rf andraxbin/* /opt/ANDRAX/bin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
